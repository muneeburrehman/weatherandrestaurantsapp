#
# Be sure to run `pod lib lint muneebsfirstpodfilecreated.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'muneebsfirstpodfilecreated'
  s.version          = '0.1.0'
  s.summary          = 'This pod is made to help you console print some confidential information you want to see'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
    This cocoa pod is to print some specific confidential information which can be uilized by swift programmers that are in the beginning phase of their development career to play with this stuff and learn pods related stuff of how it works
                       DESC

  s.homepage         = 'https://muneeburrehman@bitbucket.org/muneeburrehman/muneebsfirstpodfilecreated'
  s.screenshots     = 'https://bsongs-data.sgp1.digitaloceanspaces.com/movie_cover/Mamla-Dil-Da-Tony-Kakkar.jpg', 'https://bsongs-data.sgp1.digitaloceanspaces.com/movie_cover/Ready-For-My-Vyah-Shaadi-Anthem-Raftaar-Deep-Kalsi-Akriti-Kakar-Ft-Sonia-Mann.jpg'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'muneeburrehman103' => 'MuneeburRehman103@gmail.com' }
  s.source           = { :git => ' https://muneeburrehman@bitbucket.org/muneeburrehman/muneebsfirstpodfilecreated.git', :tag => s.version.to_s }
  s.social_media_url = 'https://www.facebook.com/Muneeburrehmann'

  s.ios.deployment_target = '12.1'

  s.source_files = 'muneebsfirstpodfilecreated/Classes/**/*'
  
  # s.resource_bundles = {
  #   'muneebsfirstpodfilecreated' => ['muneebsfirstpodfilecreated/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
   s.frameworks = 'UIKit', 'Foundation'
  # s.dependency 'AFNetworking', '~> 2.3'
end
